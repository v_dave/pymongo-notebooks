import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const Temptypes = {
    c: 'Celcius',
    f: 'Fahrenheit'
}

function FeverCheck(props){
    if(props.celcius > 36){
        return <p> You have got fever </p>
    }
    return <p> You haven't got fever </p>
}

function toCel(f){
    return (f-32) * 5/9;
}

function toFar(c){
    return (c*9/5) + 32;
}

function convert(temperature, convert){
    const input = temperature;
    if(Number.isNaN(input)){
        return '';
    }
    const output = convert(input);
    const rounded = Math.round(output);
    return rounded.toString();
}
