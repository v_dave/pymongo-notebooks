import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

function LoginButton(props){
  return(
    <div>
      <button onClick ={props.onClick}>
      Login
      </button>
    </div>
  );
}

function LogoutButton(props){
  return(
    <div>
      <button onClick ={props.onClick}>
      Logout
      </button>
    </div>
  );
}

function NumberList(props){
  const numbers = props.numbers;
  const listValues = numbers.map((value) => <li>{value}</li>);
  return (<ul>{listValues}</ul>);
}

function HelloGreeting(props){
  return <h1> hello </h1>;
}

function HiGreeting(props){
  return <h1> hi </h1>;
}

function DecideGreeting(props){
  if(props.counter % 2 === 0){
    return <HiGreeting/>;
  }
  return <HelloGreeting/>;
}

class EventHandler extends Component{
  constructor(props){
    super(props)
    this.handleLogin = this.handleLogin.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    this.state ={isloggedIn: false}  
  }

  handleLogin(params) {
    this.setState({isloggedIn: true});
  }

  handleLogout(params) {
    this.setState({isloggedIn: false});
  }

  render(){
    const isloggedIn = this.state.isloggedIn;
    let button = null;
    if(isloggedIn){
      button = <LogoutButton onClick={this.handleLogout}/>
    }
    else{
      button = <LoginButton onClick={this.handleLogin}/>
    }
    return(
      <div> 
      {button}
      </div>
    ); 
  }
}


class App extends Component {

  constructor(value){
    super(value);
    this.state = {val: 0};
  }

  componentDidMount(){
    this.timerID = setInterval(
      () => this.tick(), 5000
    );
  }

  tick(){
    this.setState({
      val: this.state.val+1 
    });
  }
  
  
  render() {
    const values = [1,2,3,4,5];
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">{this.state.val}</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
          <DecideGreeting counter = {this.state.val}/>
          <EventHandler/>          
          <NumberList numbers = {values}/>
        </p>
      </div>
    );
  }
}

export default App;
